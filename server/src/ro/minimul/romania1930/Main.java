package ro.minimul.romania1930;

public class Main {
    public static void main(String[] args) throws Exception {
        Game game = new Game();
        game.load();
        game.start();
    }
}
